<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Add Product</title>
	<link rel="stylesheet" type="text/css" href="add-product-style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/4b03424dfe.js" crossorigin="anonymous"></script>
	<script type="text/javascript" src="selector.js"></script>
</head>
<body>
<div class="container-fluid column">
	<header class="text-left">Add Product</header>
	<button type="reset" class="btn btn-primary" id="cancel-product-btn" onclick="window.location.href='index.php'"><i class="fa-solid fa-house-circle-xmark"></i> Cancel </button>
	<button class="btn btn-primary" id="save-product-btn" form="product_form" type="submit" name="submit"><i class="fa-solid fa-floppy-disk"></i> Save</button>
	<form action="add-product-action.php" id="product_form" method="post">
			<p id="sku-p">SKU:</p>
			<input type="text" id="sku" name="SKU" placeholder="SKU of The Product" required>
			<p id="name-p">Name:</p>
			<input type="text" id="name" name="name" placeholder="Name of The Product" required>
			<p id="price-p">Price:</p>
			<input type="text" id="price" name="price" placeholder="Price of The Product" required>
			<select id="productType" onchange="yesnoCheck(this);">
				<option value="select">Product Type</option>
				<option value="DVD">DVD</option>
				<option value="book">Book</option>
				<option value="furniture">Furniture</option>
			</select>
			<p id="product-type-p">Type Switcher</p>
			<div id="discSize" style="display: none;">
				<p id="size-p">Size(MB): </p> 
				<input type="text" id="size" name="size" placeholder="Size of The Disc" /><br />
			</div>
			<div id="bookWeight" style="display: none;">
				<p id="weight-p">Weight(KG): </p> 
				<input type="text" id="weight" name="weight" placeholder="Weight of The Book" /><br />
			</div>
			<div id="furnitureParameters" style="display: none;">
				<p id="height-p">Height(CM): </p> 
				<input type="text" id="height" name="height" placeholder="Height of The Furniture" /><br />
				<p id="width-p">Width(CM): </p> 
				<input type="text" id="width" name="width" placeholder="Width of The Furniture" /><br />
				<p id="length-p">Length(CM): </p> 
				<input type="text" id="length" name="length" placeholder="Length of The Furniture" /><br />
			</div>
	</form>
	<footer class="text-center">Scandiweb Test Task</footer>
</div>
</body>
</html>